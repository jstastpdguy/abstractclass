#include <iostream>
#include <vector>
#include "bicycle.h"
#include "car.h"
#include "engine.h"
#include "wheel.h"
#include "vehicle.h"
#include "WaterVehicle.h"

float getHighestPower(std::vector<vehicle*> const& v)
{
	int i = 0;
	float max_power = -1;
	
	if(v.size()){
		do
		{
			car *pointer = dynamic_cast<car*>(v[i]);

			if(pointer)
			{
				max_power = pointer->GetEnginePower();
				i++;
				break;
			}
			i++;
			
		}while(i < v.size());
	}

	for(i; i < v.size(); i++)
	{
		car *pointer = dynamic_cast<car*>(v[i]);

			if(pointer)
			{
				float curr_power = pointer->GetEnginePower();
				curr_power > max_power ? max_power = curr_power : max_power = max_power;
			}
	}

	return max_power;
}

int main()
{
	car c(engine(150), wheel(17), wheel(17), wheel(18), wheel(18), 150);	
	std::cout << c << '\n';

	bicycle t(wheel(20), wheel(20), 300);
	std::cout << t << '\n';

	std::cout << '\n';
	
	std::vector<vehicle*> v;
	v.push_back(new car(engine(150), wheel(17), wheel(17), wheel(18), wheel(18), 250));
	v.push_back(new bicycle(wheel(18), wheel(18), 7));
	v.push_back(new car(engine(200), wheel(19), wheel(19), wheel(19), wheel(19), 130));
	v.push_back(new WaterVehicle(5000));
	
	for(auto vehicle : v)
	{
		std::cout << *vehicle << '\n';
	}

	std::cout << "The highest power is " << getHighestPower(v) << '\n';
	std::cout << std::endl << "-----\n";
	
	for (int i = v.size(); i; i--){
		std::cout << '\n' << v.size() << '\n';
		delete v.at(i-1);
		v.pop_back();
	}	
	return 0;
}

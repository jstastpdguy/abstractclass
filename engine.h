#pragma once
#include <ostream>

class engine
{
	float power;
public:
	engine(float);

	float GetPower() const { return power; };
	friend std::ostream& operator << (std::ostream &, engine const &);
};

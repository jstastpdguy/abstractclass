#include "wheel.h"

wheel::wheel(float _diameter)
{
	diameter = _diameter;
}

std::ostream& operator<<(std::ostream& os, wheel const& wheel)
{
	return os << wheel.diameter;
}

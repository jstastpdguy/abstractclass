#include "vehicle.h"

#include <iostream>

std::ostream& operator << (std::ostream & os, vehicle const & vehicle)
{
	return vehicle.print(os);
}

vehicle::~vehicle()
{
	std::cout << "vehicle\n";
}

#pragma once
#include <ostream>

class vehicle
{
public:
	friend std::ostream& operator << (std::ostream& os, vehicle const & vehicle);
	virtual std::ostream& print(std::ostream &) const = 0;

	virtual ~vehicle();
};


#include "engine.h"

engine::engine(float _power)
{
	power = _power;
}

std::ostream& operator<<(std::ostream& os, engine const& engine)
{
	return os << engine.power;
}

#pragma once
#include "vehicle.h"

class WaterVehicle : public vehicle
{
	float draught;
public:
	WaterVehicle(float _draught) : draught(_draught) {};
	std::ostream& print(std::ostream &) const override;

	virtual ~WaterVehicle();
};
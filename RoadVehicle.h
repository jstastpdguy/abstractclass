#pragma once
#include "vehicle.h"

class RoadVehicle : public vehicle
{
	float clearance;
public:
	RoadVehicle(float _clearance) : clearance(_clearance) {};
	std::ostream& print(std::ostream &) const override;

	virtual ~RoadVehicle();
};

#pragma once
#include "RoadVehicle.h"

class wheel;

class bicycle : public RoadVehicle
{
	wheel * forward = nullptr;
	wheel * backward = nullptr;

public:
	bicycle( wheel, wheel, float);
	virtual ~bicycle();

	std::ostream& print(std::ostream &) const override;
};
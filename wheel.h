#pragma once
#include <ostream>

class wheel
{
	float diameter;
public:
	wheel(float);

	friend std::ostream& operator << (std::ostream&, wheel const &);
};

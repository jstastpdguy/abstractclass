#pragma once
#include "RoadVehicle.h"
#include <ostream>

#include "engine.h"

class RoadVehicle;
class wheel;
class engine;

class car : public RoadVehicle
{
	wheel * f_left;
	wheel * f_right;
	wheel * b_left;
	wheel * b_right;

	engine * engine_power;

public:
	car(engine, wheel, wheel, wheel, wheel, float);
	virtual ~car();

	float GetEnginePower() const { return engine_power->GetPower(); };
	
	std::ostream& print(std::ostream &) const override;
};

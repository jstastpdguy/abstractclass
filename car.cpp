#include "car.h"

#include <iostream>

#include "engine.h"
#include "wheel.h"
#include "RoadVehicle.h"

car::car(engine _engine, wheel _f_left, wheel _f_right, wheel _b_left, wheel _b_right, float _clearance) : RoadVehicle(_clearance)
{
	engine_power = new engine(_engine);
	f_left = new wheel(_f_left);
	f_right = new wheel(_f_right);
	b_left = new wheel(_b_left);
	b_right = new wheel(_b_right);
}

car::~car()
{
	std::cout << "car\n";
	delete engine_power;
	delete f_left;
	delete f_right;
	delete b_left;
	delete b_right;
}

std::ostream& car::print(std::ostream& os) const
{
	os << "Car Engine: " << *engine_power << " Wheels: " << *f_left << ' '
	<< *f_right << ' ' << *b_left << ' ' << *b_right << " Ride height: ";
	RoadVehicle::print(os);
	return os;
}
 
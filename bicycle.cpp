#include "bicycle.h"

#include <iostream>

#include "wheel.h"

bicycle::bicycle(wheel _forward, wheel _backward, float clearance) : RoadVehicle(clearance)
{
	forward = new wheel(_forward);
	backward = new wheel(_backward);
}

bicycle::~bicycle()
{
	std::cout << "bicycle\n";
	delete forward;
	delete backward;
}

std::ostream& bicycle::print(std::ostream& os) const
{
	os << "Bicycle " << "Wheels: " << *forward << ' ' << *backward << " Ride height: ";
	RoadVehicle::print(os);
	return os;
}
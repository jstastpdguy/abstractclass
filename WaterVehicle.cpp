#include "WaterVehicle.h"

#include <iostream>

std::ostream& WaterVehicle::print(std::ostream& os) const
{
	return os << draught;
}

WaterVehicle::~WaterVehicle()
{
	std::cout << "water\n";
}
